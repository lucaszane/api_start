using System;
using System.Collections.Generic;
using System.Text;

namespace Geographic.Domain.Bairros.Models
{
    public class BairroItem
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
    }
}
