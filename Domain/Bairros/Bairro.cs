using System;
using System.Collections.Generic;
using Geographic.Domain.Concorrentes;

namespace Geographic.Domain.Bairros
{
    public class Bairro
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Municipio { get; set; }
        public string UF { get; set; }
        public float Area { get; set; }
        public uint? QuantHabitantes { get; set; }
        public virtual IEnumerable<Concorrente> Concorrentes { get; set; }
    }
}

