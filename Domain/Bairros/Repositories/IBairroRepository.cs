using Geographic.Domain.Bairros.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Geographic.Domain.Bairros.Repositories
{
    public interface IBairroRepository
    {
        BairroItem GetById(string id);
    }
}
