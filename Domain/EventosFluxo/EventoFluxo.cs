using System;
using Geographic.Domain.Bairros;
using Geographic.Domain.Concorrentes;

namespace Geographic.Domain.EventosFluxo
{
    public class EventoFluxo
    {
        public string Codigo { get; set; }
        public DateTime DataEvento { get; set; }
        public string CodConcorrente { get; set; }
        public Concorrente Concorrente { get; set; }

    }
}

