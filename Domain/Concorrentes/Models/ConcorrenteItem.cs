


using System.Collections.Generic;

namespace geographic.Domain.Concorrentes.Models
{
    public class ConcorrenteItem
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public int FaixaPreco { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public uint Populacao { get; set; }
        public float DensidadeDemográfica { get; set; }
        public IEnumerable<FluxoItem> FluxoPorDiaDaSemana { get; set; }
        public IEnumerable<FluxoItem> FluxoPorPeriodo { get; set; }

    }
}