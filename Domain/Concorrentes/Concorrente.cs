using System;
using System.Collections.Generic;
using Geographic.Domain.Bairros;
using Geographic.Domain.EventosFluxo;

namespace Geographic.Domain.Concorrentes
{
    public class Concorrente
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public int FaixaPreco { get; set; }
        public string Endereco { get; set; }
        public string Municipio { get; set; }
        public string UF { get; set; }
        public string CodBairro { get; set; }
        public Bairro Bairro { get; set; }
        public virtual IEnumerable<EventoFluxo> EventosFluxo { get; set; }

    }
}

