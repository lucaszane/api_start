
using System.Threading.Tasks;

namespace geographic.Domain.Concorrentes.Repositories
{
    public interface IConcorrenteRepository
    {
        object GetConcorrentById(string id);
        Task<object> GetAllConcorrentes();
    }
}