﻿CREATE DATABASE GeographicDb;

IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Bairros] (
    [Codigo] nvarchar(450) NOT NULL,
    [Nome] nvarchar(max) NULL,
    [Municipio] nvarchar(max) NULL,
    [UF] nvarchar(max) NULL,
    [Area] bigint NOT NULL,
    CONSTRAINT [PK_Bairros] PRIMARY KEY ([Codigo])
);

GO

CREATE TABLE [Users] (
    [Id] uniqueidentifier NOT NULL,
    [UserName] nvarchar(max) NULL,
    [Password] nvarchar(max) NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Concorrentes] (
    [Codigo] nvarchar(450) NOT NULL,
    [Nome] nvarchar(max) NULL,
    [Categoria] nvarchar(max) NULL,
    [FaixaPreco] int NOT NULL,
    [Endereco] nvarchar(max) NULL,
    [Municipio] nvarchar(max) NULL,
    [UF] nvarchar(max) NULL,
    [CodBairro] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_Concorrentes] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_Concorrentes_Bairros_CodBairro] FOREIGN KEY ([CodBairro]) REFERENCES [Bairros] ([Codigo]) ON DELETE CASCADE
);

GO

CREATE TABLE [Populacoes] (
    [CodigoBairro] nvarchar(450) NOT NULL,
    [QuantHabitantes] bigint NOT NULL,
    CONSTRAINT [PK_Populacoes] PRIMARY KEY ([CodigoBairro]),
    CONSTRAINT [FK_Populacoes_Bairros_CodigoBairro] FOREIGN KEY ([CodigoBairro]) REFERENCES [Bairros] ([Codigo]) ON DELETE CASCADE
);

GO

CREATE TABLE [EventosFluxo] (
    [Codigo] nvarchar(450) NOT NULL,
    [DataEvento] datetime2 NOT NULL,
    [CodConcorrente] nvarchar(450) NULL,
    CONSTRAINT [PK_EventosFluxo] PRIMARY KEY ([Codigo]),
    CONSTRAINT [FK_EventosFluxo_Concorrentes_CodConcorrente] FOREIGN KEY ([CodConcorrente]) REFERENCES [Concorrentes] ([Codigo]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Concorrentes_CodBairro] ON [Concorrentes] ([CodBairro]);

GO

CREATE INDEX [IX_EventosFluxo_CodConcorrente] ON [EventosFluxo] ([CodConcorrente]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190319000747_InitialMigration', N'3.0.0-preview.18572.1');

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Bairros]') AND [c].[name] = N'Area');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Bairros] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Bairros] ALTER COLUMN [Area] real NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190323163329_BairroFieldChange', N'3.0.0-preview.18572.1');

GO

ALTER TABLE [Concorrentes] DROP CONSTRAINT [FK_Concorrentes_Bairros_CodBairro];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Concorrentes]') AND [c].[name] = N'CodBairro');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Concorrentes] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Concorrentes] ALTER COLUMN [CodBairro] nvarchar(450) NULL;

GO

ALTER TABLE [Concorrentes] ADD CONSTRAINT [FK_Concorrentes_Bairros_CodBairro] FOREIGN KEY ([CodBairro]) REFERENCES [Bairros] ([Codigo]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190323200444_ConcorrenteBairroNullable', N'3.0.0-preview.18572.1');

GO

ALTER TABLE [EventosFluxo] DROP CONSTRAINT [PK_EventosFluxo];

GO

ALTER TABLE [EventosFluxo] ADD CONSTRAINT [PK_EventosFluxo] PRIMARY KEY ([Codigo], [DataEvento]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190324024841_EventoFluxoCompositeKeyAdd', N'3.0.0-preview.18572.1');

GO

ALTER TABLE [Bairros] ADD [QuantHabitantes] bigint NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190324025513_BairroTableChange', N'3.0.0-preview.18572.1');

GO

DROP TABLE [Populacoes];

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190324025931_PopulacaoRemove', N'3.0.0-preview.18572.1');

GO

ALTER TABLE [EventosFluxo] DROP CONSTRAINT [FK_EventosFluxo_Concorrentes_CodConcorrente];

GO

DROP TABLE [Users];

GO

ALTER TABLE [EventosFluxo] DROP CONSTRAINT [PK_EventosFluxo];

GO

DROP INDEX [IX_EventosFluxo_CodConcorrente] ON [EventosFluxo];
DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EventosFluxo]') AND [c].[name] = N'CodConcorrente');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [EventosFluxo] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [EventosFluxo] ALTER COLUMN [CodConcorrente] nvarchar(450) NOT NULL;
CREATE INDEX [IX_EventosFluxo_CodConcorrente] ON [EventosFluxo] ([CodConcorrente]);

GO

ALTER TABLE [EventosFluxo] ADD CONSTRAINT [PK_EventosFluxo] PRIMARY KEY ([Codigo], [DataEvento], [CodConcorrente]);

GO

ALTER TABLE [EventosFluxo] ADD CONSTRAINT [FK_EventosFluxo_Concorrentes_CodConcorrente] FOREIGN KEY ([CodConcorrente]) REFERENCES [Concorrentes] ([Codigo]) ON DELETE CASCADE;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190324211627_ConcorrentKeyChange', N'3.0.0-preview.18572.1');

GO

