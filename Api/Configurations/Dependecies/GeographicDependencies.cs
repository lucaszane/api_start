using System;
using Microsoft.Extensions.DependencyInjection;
using Geographic.Infra.Context;
using geographic.Infra.Repositories;
using geographic.Domain.Concorrentes.Repositories;

namespace Geographic.Api.Configurations.Dependencies
{
    public static class GeographicDependencies
    {
        public static IServiceCollection AddGeographicDependencies(this IServiceCollection services)
        {

            // Repositories

            services.AddScoped<IConcorrenteRepository, ConcorrenteRepository>();


            return services;
        }
    }
}
