﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using geographic.Domain.Concorrentes.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace geographic.api.Controllers
{
    [Route("api/concorrente")]
    [Controller]
    public class ValuesController : ControllerBase
    {
        public IConcorrenteRepository _concorrenteRepository;
        public ValuesController(IConcorrenteRepository concorrenteRepository)
        {
            _concorrenteRepository = concorrenteRepository;
        }

        // GET api/values
        [HttpGet]
        public async Task<object> Get()
        {
            return Ok(await _concorrenteRepository.GetAllConcorrentes());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public object Get(string id)
        {
            return _concorrenteRepository.GetConcorrentById(id);
        }
    }
}
