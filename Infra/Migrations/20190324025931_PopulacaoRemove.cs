﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Migrations
{
    public partial class PopulacaoRemove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Populacoes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Populacoes",
                columns: table => new
                {
                    CodigoBairro = table.Column<string>(nullable: false),
                    QuantHabitantes = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Populacoes", x => x.CodigoBairro);
                    table.ForeignKey(
                        name: "FK_Populacoes_Bairros_CodigoBairro",
                        column: x => x.CodigoBairro,
                        principalTable: "Bairros",
                        principalColumn: "Codigo",
                        onDelete: ReferentialAction.Cascade);
                });
        }
    }
}
