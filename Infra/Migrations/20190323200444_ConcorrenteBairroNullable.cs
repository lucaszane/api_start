﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Migrations
{
    public partial class ConcorrenteBairroNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Concorrentes_Bairros_CodBairro",
                table: "Concorrentes");

            migrationBuilder.AlterColumn<string>(
                name: "CodBairro",
                table: "Concorrentes",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_Concorrentes_Bairros_CodBairro",
                table: "Concorrentes",
                column: "CodBairro",
                principalTable: "Bairros",
                principalColumn: "Codigo",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Concorrentes_Bairros_CodBairro",
                table: "Concorrentes");

            migrationBuilder.AlterColumn<string>(
                name: "CodBairro",
                table: "Concorrentes",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Concorrentes_Bairros_CodBairro",
                table: "Concorrentes",
                column: "CodBairro",
                principalTable: "Bairros",
                principalColumn: "Codigo",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
