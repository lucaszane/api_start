﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Migrations
{
    public partial class EventoFluxoCompositeKeyAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo",
                columns: new[] { "Codigo", "DataEvento" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo",
                column: "Codigo");
        }
    }
}
