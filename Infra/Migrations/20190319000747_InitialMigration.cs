﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bairros",
                columns: table => new
                {
                    Codigo = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Municipio = table.Column<string>(nullable: true),
                    UF = table.Column<string>(nullable: true),
                    Area = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bairros", x => x.Codigo);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Concorrentes",
                columns: table => new
                {
                    Codigo = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Categoria = table.Column<string>(nullable: true),
                    FaixaPreco = table.Column<int>(nullable: false),
                    Endereco = table.Column<string>(nullable: true),
                    Municipio = table.Column<string>(nullable: true),
                    UF = table.Column<string>(nullable: true),
                    CodBairro = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Concorrentes", x => x.Codigo);
                    table.ForeignKey(
                        name: "FK_Concorrentes_Bairros_CodBairro",
                        column: x => x.CodBairro,
                        principalTable: "Bairros",
                        principalColumn: "Codigo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Populacoes",
                columns: table => new
                {
                    CodigoBairro = table.Column<string>(nullable: false),
                    QuantHabitantes = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Populacoes", x => x.CodigoBairro);
                    table.ForeignKey(
                        name: "FK_Populacoes_Bairros_CodigoBairro",
                        column: x => x.CodigoBairro,
                        principalTable: "Bairros",
                        principalColumn: "Codigo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EventosFluxo",
                columns: table => new
                {
                    Codigo = table.Column<string>(nullable: false),
                    DataEvento = table.Column<DateTime>(nullable: false),
                    CodConcorrente = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventosFluxo", x => x.Codigo);
                    table.ForeignKey(
                        name: "FK_EventosFluxo_Concorrentes_CodConcorrente",
                        column: x => x.CodConcorrente,
                        principalTable: "Concorrentes",
                        principalColumn: "Codigo",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Concorrentes_CodBairro",
                table: "Concorrentes",
                column: "CodBairro");

            migrationBuilder.CreateIndex(
                name: "IX_EventosFluxo_CodConcorrente",
                table: "EventosFluxo",
                column: "CodConcorrente");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventosFluxo");

            migrationBuilder.DropTable(
                name: "Populacoes");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Concorrentes");

            migrationBuilder.DropTable(
                name: "Bairros");
        }
    }
}
