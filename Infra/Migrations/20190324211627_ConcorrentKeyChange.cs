﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Migrations
{
    public partial class ConcorrentKeyChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EventosFluxo_Concorrentes_CodConcorrente",
                table: "EventosFluxo");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo");

            migrationBuilder.AlterColumn<string>(
                name: "CodConcorrente",
                table: "EventosFluxo",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo",
                columns: new[] { "Codigo", "DataEvento", "CodConcorrente" });

            migrationBuilder.AddForeignKey(
                name: "FK_EventosFluxo_Concorrentes_CodConcorrente",
                table: "EventosFluxo",
                column: "CodConcorrente",
                principalTable: "Concorrentes",
                principalColumn: "Codigo",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EventosFluxo_Concorrentes_CodConcorrente",
                table: "EventosFluxo");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo");

            migrationBuilder.AlterColumn<string>(
                name: "CodConcorrente",
                table: "EventosFluxo",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_EventosFluxo",
                table: "EventosFluxo",
                columns: new[] { "Codigo", "DataEvento" });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_EventosFluxo_Concorrentes_CodConcorrente",
                table: "EventosFluxo",
                column: "CodConcorrente",
                principalTable: "Concorrentes",
                principalColumn: "Codigo",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
