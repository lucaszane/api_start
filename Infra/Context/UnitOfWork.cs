using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Geographic.Infra.Context
{
    public class UnitOfWork
    {
        private readonly GeographicContext _geographicContext;

        public UnitOfWork(GeographicContext GeographicContext)
        {
            _geographicContext = GeographicContext;
        }

        public async Task<bool> CommitAsync()
        {
            var rowsAffected = await _geographicContext.SaveChangesAsync();
            return rowsAffected > 0;
        }
    }
}
