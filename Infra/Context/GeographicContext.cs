using System;
using Geographic.Domain.Bairros;
using Geographic.Domain.EventosFluxo;
using Geographic.Domain.Concorrentes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Geographic.Infra.Context
{
    public class GeographicContext : DbContext
    {
        public GeographicContext(DbContextOptions<GeographicContext> options)
            : base(options)
        {
        }
        public DbSet<Bairro> Bairros { get; set; }
        public DbSet<EventoFluxo> EventosFluxo { get; set; }
        public DbSet<Concorrente> Concorrentes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ConfigureBairro(modelBuilder.Entity<Bairro>());
            ConfigureEventoFluxo(modelBuilder.Entity<EventoFluxo>());
            ConfigureConcorrente(modelBuilder.Entity<Concorrente>());
        }

        private void ConfigureBairro(EntityTypeBuilder<Bairro> builder)
        {
            builder
                .HasKey(t => t.Codigo);
        }

        private void ConfigureEventoFluxo(EntityTypeBuilder<EventoFluxo> builder)
        {
            builder
                .HasKey(t => new {t.Codigo, t.DataEvento, t.CodConcorrente });

            builder.HasOne(c => c.Concorrente).WithMany(e => e.EventosFluxo).HasForeignKey(k => k.CodConcorrente);
        }

        private void ConfigureConcorrente(EntityTypeBuilder<Concorrente> builder)
        {
            builder
                .HasKey(t => t.Codigo);

            builder.HasOne(c => c.Bairro).WithMany(e => e.Concorrentes).HasForeignKey(k => k.CodBairro).IsRequired(false);
        }
    }
}
