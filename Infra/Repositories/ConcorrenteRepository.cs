
using System.Linq;
using System.Threading.Tasks;
using geographic.Domain.Concorrentes.Models;
using geographic.Domain.Concorrentes.Repositories;
using Geographic.Domain.Concorrentes;
using Geographic.Infra.Context;
using Microsoft.EntityFrameworkCore;

namespace geographic.Infra.Repositories
{
    public class ConcorrenteRepository : IConcorrenteRepository
    {
        private readonly GeographicContext Context;
        private readonly DbSet<Concorrente> _concorrentes;

        public ConcorrenteRepository(GeographicContext context)
        {
            Context = context;
            Context.Database.SetCommandTimeout(300);
            _concorrentes = context.Concorrentes;
        }
        public async Task<object> GetAllConcorrentes()
        {
            var result = await _concorrentes
            .Include(c => c.Bairro)
            .Include(c => c.EventosFluxo)
            .Select(c => new ConcorrenteItem()
            {
                Codigo = c.Categoria,
                Nome = c.Nome,
                FaixaPreco = c.FaixaPreco,
                Endereco = c.Endereco,
                Bairro = c.Bairro != null ? c.Bairro.Nome : "",
                Populacao = c.Bairro != null ? c.Bairro.QuantHabitantes.Value : 0,
                DensidadeDemográfica = c.Bairro != null && c.Bairro.QuantHabitantes != null ? c.Bairro.QuantHabitantes.Value / c.Bairro.Area : 0,
                FluxoPorDiaDaSemana = c.EventosFluxo.GroupBy(e => e.DataEvento.DayOfWeek).Select(e => new FluxoItem()
                {
                    Unidade = e.Key.ToString(),
                    FluxoMedio = e.Count()
                }),
                FluxoPorPeriodo = c.EventosFluxo.GroupBy(e => e.DataEvento.Hour < 12 ? "Manhã" : "Tarde").Select(e => new FluxoItem()
                {
                    Unidade = e.Key.ToString(),
                    FluxoMedio = e.Count()
                }),
            }).ToListAsync();

            return result;
        }

        public object GetConcorrentById(string id)
        {
            var result = _concorrentes
            .Where(c => c.Codigo == id)
            .Include(c => c.Bairro)
            .Include(c => c.EventosFluxo)
            .Select(c => new ConcorrenteItem()
            {
                Codigo = c.Categoria,
                Nome = c.Nome,
                FaixaPreco = c.FaixaPreco,
                Endereco = c.Endereco,
                Bairro = c.Bairro.Nome,
                Populacao = c.Bairro.QuantHabitantes.Value,
                DensidadeDemográfica = c.Bairro.QuantHabitantes.Value / c.Bairro.Area,
                FluxoPorDiaDaSemana = c.EventosFluxo.GroupBy(e => e.DataEvento.DayOfWeek).ToList().Select(e => new FluxoItem()
                {
                    Unidade = e.Key.ToString(),
                    FluxoMedio = e.Count()
                }),
                FluxoPorPeriodo = c.EventosFluxo.GroupBy(e => e.DataEvento.Hour < 12 ? "Manhã" : "Tarde").ToList().Select(e => new FluxoItem()
                {
                    Unidade = e.Key.ToString(),
                    FluxoMedio = e.Count()
                }),
            });

            return result;
        }
    }
}